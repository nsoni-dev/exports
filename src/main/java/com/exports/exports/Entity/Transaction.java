package com.exports.exports.Entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "transactions")
@NoArgsConstructor

public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int userId;

    @ManyToOne
    @JoinColumn(name = "payment_method_id")
    private PaymentMethod paymentMethod;

    @Column(name = "transaction_type")
    private int transactionType;

    @Column(name = "initial_amount")
    private BigDecimal initialAmount;

    @Column(name = "final_amount")
    private BigDecimal finalAmount;

    @Column(name = "processed_date")
    private LocalDateTime processedDate;

    @Column(name = "processed_by")
    private int processedBy;

    @Column(name = "payment_source")
    private int paymentSource;

    @Column(name = "payment_mode")
    private int paymentMode;

    @Column(name = "wallet_id")
    private int walletId;

    @Column(name = "funnel_id")
    private int funnelId;

    @Column(name = "status")
    private int status;

    @Column(name = "extra_column_1")
    private int extraColumn1;

    @Column(name = "extra_column_2")
    private int extraColumn2;

    @Column(name = "extra_column_3")
    private int extraColumn3;

    @ManyToOne
    @JoinColumn(name = "current_manager")
    private User user;

    @Column(name = "manager_change")
    private int managerChange;

    // Constructors, getters, setters

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public BigDecimal getFinalAmount() {
        return finalAmount;
    }

    public LocalDateTime getProcessedDate() {
        return processedDate;
    }

    public int getProcessedBy() {
        return processedBy;
    }

    public int getPaymentSource() {
        return paymentSource;
    }

    public int getPaymentMode() {
        return paymentMode;
    }

    public int getWalletId() {
        return walletId;
    }

    public int getFunnelId() {
        return funnelId;
    }

    public int getStatus() {
        return status;
    }

    public int getExtraColumn1() {
        return extraColumn1;
    }

    public int getExtraColumn2() {
        return extraColumn2;
    }

    public int getExtraColumn3() {
        return extraColumn3;
    }

    public User getUser() {
        return user;
    }

    public int getManagerChange() {
        return managerChange;
    }
}