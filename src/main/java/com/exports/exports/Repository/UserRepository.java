package com.exports.exports.Repository;

import com.exports.exports.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
public interface UserRepository extends JpaRepository<User, Long> {
    User findByIdAndTransactionsId(Long userId, Long transactionId);
}