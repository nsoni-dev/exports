package com.exports.exports.Controller;

import com.exports.exports.Entity.Transaction;
import com.exports.exports.Service.TransactionService;
import com.exports.exports.dto.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController  {

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/list")
    public List<TransactionDTO> getAllTransactions() {
        return transactionService.getAllTransactions();
    }
}
