package com.exports.exports.dto;

import java.time.LocalDateTime;

public class TransactionDTO {
    private int id;
    private int userId;
    private String paymentmethodName;
    private int transactionType;
    private double initialAmount;
    private double finalAmount;
    private LocalDateTime processedDate;
    private int processedBy;
    private int paymentSource;
    private int paymentMode;
    private int walletId;
    private int funnelId;
    private int status;
    private int extraColumn1;
    private int extraColumn2;
    private int extraColumn3;
    private String userName;
    private String userEmail;
    private String userCountry;
    private int managerChange;

    // Constructor
// Constructor
    public TransactionDTO(int id, int userId, String paymentmethodName, int transactionType,
                          double initialAmount, double finalAmount, LocalDateTime processedDate,
                          int processedBy, int paymentSource, int paymentMode, int walletId,
                          int funnelId, int status, int extraColumn1, int extraColumn2,
                          int extraColumn3, String userName, String userEmail,
                          String userCountry, int managerChange) {
        this.id = id;
        this.userId = userId;
        this.paymentmethodName = paymentmethodName;
        this.transactionType = transactionType;
        this.initialAmount = initialAmount;
        this.finalAmount = finalAmount;
        this.processedDate = processedDate;
        this.processedBy = processedBy;
        this.paymentSource = paymentSource;
        this.paymentMode = paymentMode;
        this.walletId = walletId;
        this.funnelId = funnelId;
        this.status = status;
        this.extraColumn1 = extraColumn1;
        this.extraColumn2 = extraColumn2;
        this.extraColumn3 = extraColumn3;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userCountry = userCountry;
        this.managerChange = managerChange;
    }
}
