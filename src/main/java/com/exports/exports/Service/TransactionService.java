package com.exports.exports.Service;

import com.exports.exports.dto.TransactionDTO;
import com.exports.exports.Entity.Transaction;
import com.exports.exports.Repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public List<TransactionDTO> getAllTransactions() {
        List<Transaction> transactions = transactionRepository.findAll();

        return transactions.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    private TransactionDTO convertToDTO(Transaction transaction) {

        // Assuming you have a method or constructor in TransactionDTO to convert Transaction to TransactionDTO
        return new TransactionDTO(
                transaction.getId(),
                transaction.getUserId(),
                transaction.getPaymentMethod().getName(),
                transaction.getTransactionType(),
                transaction.getInitialAmount().doubleValue(), // Convert BigDecimal to double
                transaction.getFinalAmount().doubleValue(),   // Convert BigDecimal to double
                transaction.getProcessedDate(),
                transaction.getProcessedBy(),
                transaction.getPaymentSource(),
                transaction.getPaymentMode(),
                transaction.getWalletId(),
                transaction.getFunnelId(),
                transaction.getStatus(),
                transaction.getExtraColumn1(),
                transaction.getExtraColumn2(),
                transaction.getExtraColumn3(),
                transaction.getUser().getName(),
                transaction.getUser().getEmail(),
                transaction.getUser().getCountry().getName(),
                transaction.getManagerChange()
        );

    }


}
